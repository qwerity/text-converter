#include <gtk/gtk.h>
#include <iconv.h>
#include <glade/glade.h>
#include <libxml/parser.h>
#include <libxml/xpath.h>
#include <errno.h>

#include <iostream>
using namespace std;

// conversation descriptor
struct mode
{
	string From;
	string To;
};

GtkWidget *about, *dialog, *window;
GtkComboBox *cbox;
GtkTextView *text_view;
GtkTextBuffer *buffer;
GtkTextIter start, end;
GdkPixbuf * logo;

string DataDir;
GtkBuilder *xml;
iconv_t curr_cd = NULL; // current conversation descriptor
mode *encode = new mode[5];
gint enc_index; // encode enc_index
size_t out_size; // converted text size
string out_text; // converted text
gboolean enc_type = TRUE;
gboolean b_is_converted = FALSE; 



gboolean is_converted()
{
	return b_is_converted;
}

extern "C" void on_window_selection_get ( GtkWidget *widget, 
										   GtkSelectionData *data,
										   guint             info,
										   guint             time,
  										   gpointer       user_data)
{
	
	cout << "Responding to selection get" << endl;
	cerr << "Out_Text: " << out_text << endl;
	
	if (out_size < 0 || out_text.empty())
		return;
		
	if(enc_type)
		gtk_selection_data_set(data, GDK_SELECTION_TYPE_STRING, 8, (const guchar*)out_text.c_str(), out_size);
	else
		gtk_selection_data_set_text(data, out_text.c_str(), out_size);
}

extern "C" void on_btnCopy_clicked (GtkButton *button, gpointer user_data)
{
	if(!is_converted())
	{
		GtkWidget * dlg = gtk_message_dialog_new_with_markup(GTK_WINDOW(window), 
				GTK_DIALOG_MODAL, GTK_MESSAGE_INFO, GTK_BUTTONS_OK, 
				"First Convert, then try to Copy.");
				
		gtk_dialog_run(GTK_DIALOG(dlg));
		gtk_widget_destroy(dlg);
		return;
	}
	
	GdkAtom Clipboard = gdk_atom_intern("CLIPBOARD", FALSE);
	
	if ( Clipboard == GDK_NONE )
	{
		cerr << "Cannot create clipboard atom" << endl;
		return;
	}
	
	if (out_size < 0 || out_text.empty())
		return;
	
	gtk_selection_add_target(window, Clipboard, gdk_atom_intern("STRING", TRUE), 0);
	gtk_selection_add_target(window, Clipboard, gdk_atom_intern("UTF8_STRING", TRUE), 1);
	
	if ( gtk_selection_owner_set(GTK_WIDGET(window), Clipboard, GDK_CURRENT_TIME) == FALSE)
		cerr << "Cannot set selection owner" << endl;
}

extern "C" void on_textview_copy_clipboard (GtkWidget *textview, gpointer user_data)
{
	on_btnCopy_clicked(NULL, NULL);
}

extern "C" void on_btnConvert_clicked (GtkButton *button, gpointer user_data)
{
	string in_text;
	char* tmp = NULL;
	size_t in_size = 0;
	
	if (curr_cd == NULL)
	{
		GtkWidget * dlg = gtk_message_dialog_new_with_markup(GTK_WINDOW(window), 
				GTK_DIALOG_MODAL, GTK_MESSAGE_INFO, GTK_BUTTONS_OK, 
				"\nYou must at first set the encoding type");
				
		gtk_dialog_run(GTK_DIALOG(dlg));
		gtk_widget_destroy(dlg);
		return;
	}
			
	b_is_converted = TRUE;
	
	gtk_text_buffer_get_bounds(buffer, &start, &end);
	in_text = gtk_text_buffer_get_text(buffer, &start, &end,  FALSE);
	
	in_size = in_text.length();
	
	out_size = 2 * in_size;
	
		
	//Covert
	if(enc_type)
		{
			tmp = g_convert(in_text.c_str(), in_size, encode[enc_index].To.c_str(), encode[enc_index].From.c_str(), NULL, &out_size, NULL);
			if(tmp == NULL)
			{
				cout << "Cannot convert! - " << "Error code: " << errno << endl;		
				return;
			}
			
			out_text = tmp;
			free(tmp);
		}
	else
		{			
			char * tmp = new char[in_size];
			
			int i = 0,  j = 0;
			
			for (; i < (int)in_size; ++i)
			{
				if ( in_text[i] + 256 == 194 )
				{
					if ( in_text[i+1] + 256 == 168 ) 
					{
						tmp[j++] = in_text[++i] - 6;
					}
					else
						tmp[j++] = in_text[++i];
				}
				else if ( in_text[i] + 256 == 195)
					tmp[j++] = in_text[++i] + 64;
				else if ( in_text[i] + 256 == 213)
						{
							if(in_text[i+1] + 256 == 158)
								tmp[j++] = in_text[++i] + 19;
							else if(in_text[i+1] + 256 == 157)
									tmp[j++] = in_text[++i] + 13;
							else if(in_text[i+1] + 256 == 155)
									tmp[j++] = in_text[++i] + 21;
						}
					
				else if ( in_text[i] + 256 == 214)
					tmp[j++] = in_text[++i] + 26;
				else if ( in_text[i] + 256 == 226 && in_text[i+1] + 256 == 128 && in_text[i+2] + 256 == 166)
					{
						i += 2;
						tmp[j++] = in_text[i] + 8;
						
					}
				else if ( in_text[i] + 256 == 226 && in_text[i+1] + 256 == 128 && in_text[i+2] + 256 == 147)
					{
						i += 2;
						tmp[j++] = in_text[i] + 21;						
					}
				else if ( in_text[i] + 256 == 226 && in_text[i+1] + 256 == 128 && in_text[i+2] + 256 == 153)
				{
					tmp[j++] = in_text[i+2] + 101;
					i += 2;
				}
				else if ( in_text[i] + 256 == 226 && in_text[i+1] + 256 == 128 && in_text[i+2] + 256 == 148)
				{
					tmp[j++] = in_text[i+2] + 20;
					i += 2;
				}				
				else
					tmp[j++] = in_text[i];
			}
			tmp[j] = 0;
			
			in_text = tmp;
			in_size = j;
						
			tmp = g_convert(in_text.c_str(), in_size, encode[enc_index].To.c_str(), encode[enc_index].From.c_str(), NULL, &out_size, NULL);
			if(tmp == NULL)
			{
				cout << "Cannot convert! - " << "Error code: " << errno << endl;		
				return;
			}
			
			out_text = tmp;
			free(tmp);
			
			gtk_text_buffer_set_text(buffer, out_text.c_str(), out_size);
		}
		
	cout << "Converted: "<< out_text << "\nstrlen: " << out_text.length() << "\nsize: " << out_size << endl;
	
	GtkWidget * dlg = gtk_message_dialog_new_with_markup(GTK_WINDOW(window), GTK_DIALOG_MODAL, GTK_MESSAGE_INFO, GTK_BUTTONS_OK, "The text is converted successfully. \nPress 'Copy' to copy it.");
	gtk_dialog_run(GTK_DIALOG(dlg));
	gtk_widget_destroy(dlg);
}

extern "C" void on_quit_activate ()
{
	gtk_main_quit();
}

extern "C" void on_new_activate ()
{
	gtk_text_buffer_set_text(buffer, "", 0);
}

extern "C" void on_save_activate ()
{
	FILE *f;
	
	dialog = gtk_file_chooser_dialog_new("Save Text", GTK_WINDOW(window), 
					GTK_FILE_CHOOSER_ACTION_SAVE, GTK_STOCK_SAVE, GTK_RESPONSE_ACCEPT,
					GTK_STOCK_CANCEL, GTK_RESPONSE_CANCEL, NULL);
	gtk_file_chooser_set_do_overwrite_confirmation(GTK_FILE_CHOOSER(dialog), TRUE);
	gtk_file_chooser_set_current_name (GTK_FILE_CHOOSER (dialog), "Untitled document");
	gtk_file_chooser_set_current_folder(GTK_FILE_CHOOSER(dialog), g_get_home_dir());
	
	if (gtk_dialog_run (GTK_DIALOG (dialog)) == GTK_RESPONSE_ACCEPT)
	{
		string filename;
		size_t tf;
		
		gtk_text_buffer_get_bounds(buffer, &start, &end);
		string text = gtk_text_buffer_get_text(buffer, &start, &end,  FALSE);

		filename = gtk_file_chooser_get_filename (GTK_FILE_CHOOSER (dialog));

		//save_to_file (filename);
		f = fopen(filename.c_str(), "wb");
		tf = fwrite((const void*) text.c_str(), 1, text.length(), f);
		
		fclose(f);
	}
	else
	{
		gtk_widget_destroy (dialog);
	}
	
	gtk_widget_destroy (dialog);
}

void destroy(GtkWidget *w, gpointer gp)
{
	gtk_main_quit();
}

extern "C" void on_about_activate ()
{
	const char * authors[] = { "Shakhzadyan Khachik <qwerity@gmail.com>", "Vahram Martirosyan <vmartirosyan@gmail.com>", NULL};
	const char * artists[] = { "Biayna Mahari <biayna11@yahoo.com>", NULL};
	
	about = gtk_about_dialog_new();
	//gtk_widget_show_all(about);
	gtk_about_dialog_set_logo(GTK_ABOUT_DIALOG (about), logo);
	gtk_about_dialog_set_authors(GTK_ABOUT_DIALOG(about), authors);
	gtk_about_dialog_set_artists(GTK_ABOUT_DIALOG(about), artists);
	gtk_about_dialog_set_copyright(GTK_ABOUT_DIALOG(about), "System Programming Laboratory");
	gtk_about_dialog_set_name(GTK_ABOUT_DIALOG(about), "Text Converter");
	gtk_about_dialog_set_version(GTK_ABOUT_DIALOG(about), "1.0");
	gtk_about_dialog_set_program_name(GTK_ABOUT_DIALOG(about), "Text Converter");
	gtk_about_dialog_set_license (GTK_ABOUT_DIALOG (about), "Text Converter 1.0\n\n\
This program is free software; you can redistribute it\n and/or modify it under the terms of the GNU General\n Public License as published by the Free Software\n Foundation; either version 2 of the licence, or (at your\n option) any later version.\n\n\
\
This program is distributed in the hope that it will be\n useful, but WITHOUT ANY WARRANTY; without even\n the implied warranty of MERCHANTABILITY or FITNESS\n FOR A PARTICULAR PURPOSE.  See the GNU General\n Public License for more details.\n\n\
\
You should have received a copy of the GNU General\n Public License along with this program.  You may also\n obtain a copy of the GNU General Public License from\n the Free Software Foundation by visiting their web\n site (http://www.fsf.org/) or by writing to the Free\n Software Foundation, Inc., 51 Franklin St, Fifth Floor,\n Boston, MA  02110-1301  USA);	");
	
	gtk_dialog_run(GTK_DIALOG(about));
	gtk_widget_destroy(about);
}

void xmlParse(GtkComboBox *combo, string xmlFilename)
{
	xmlDocPtr file;
	xmlNodePtr curr;
	int i = 0;
	
	file = xmlParseFile(xmlFilename.c_str());
	if(file == NULL)
	{
		fprintf(stderr, "Document not parsed successfully. \n");
		return;
	}
	
	curr = xmlDocGetRootElement(file);
	if(curr == NULL)
	{
		fprintf(stderr, "Empty xml file. \n");
		return;
	}
	
	if(xmlStrcmp(curr -> name, (const xmlChar*) "Conversions"))
	{
		fprintf(stderr, "Xml file for conversation mode was crashed please replace with normal one");
		xmlFreeDoc(file);
		return;
	}
	
	curr = curr -> xmlChildrenNode;
	curr = curr -> next;
	
	while(curr != NULL)
	{
		gtk_combo_box_insert_text(combo, i, 
				(const gchar*)xmlNodeListGetString(file, curr -> xmlChildrenNode, 1));
		{
			encode[i].From = (const char*) xmlGetProp(curr, (const xmlChar*)"From");
			encode[i].To = (const char*) xmlGetProp(curr, (const xmlChar*)"To");
		}
		curr = curr -> next;
		curr = curr -> next;
		i = i + 1;
	}
}

extern "C" void on_combobox_changed(GtkComboBox *combo, gpointer data) 
{
	enc_index = gtk_combo_box_get_active(combo);
	
	if(enc_index == 0)
	{
		enc_type = FALSE;
	}
	if(enc_index == 1)
	{
		enc_type = TRUE;
	}
	if(enc_index == -1)
	{
		fprintf(stderr, "Please choose encoding type!");
	}
	else
	{		
		curr_cd = iconv_open(encode[enc_index].To.c_str(), encode[enc_index].From.c_str());
		if (curr_cd == (iconv_t) - 1)
		{
			cerr << "Cannot create convertion object: " << errno << endl;
			return;
		}
	}
}

int main(int argc, char** argv)
{
	GError *error = NULL;
	
	gtk_init (&argc, &argv);
	
	// Obtain the data dir to load the XMl and glade files.
	FILE * fd = popen("which textconverter", "r");
	
	size_t bytes_read = 100;
	char* FileName = new char[100];
	bytes_read = fread(FileName, 1, bytes_read, fd);
	
	if ( bytes_read <=  1 )
	{
		cerr << "Cannot obtain data directory" << endl;
		return -1;
	}
	fclose(fd);

	DataDir = g_path_get_dirname(FileName);
	
	DataDir = DataDir + "/../share/textconverter" ;	
	size_t length = DataDir.length();
	
	string xmlFileName = DataDir + "/Conversions.xml";		
	
	DataDir = DataDir.substr(0, length);
	
	// init
	DataDir = DataDir + "/ui.glade";
	xml = gtk_builder_new();
	if(!gtk_builder_add_from_file(xml, DataDir.c_str(), &error))
    {
        g_warning("%s", error->message);
        g_free(error);
        return(1);
    }
	
	// Logo
	DataDir = DataDir.substr(0, length);
	DataDir = DataDir + "/logo.png";
	logo = gdk_pixbuf_new_from_file(DataDir.c_str(), NULL);
	
	
	buffer = gtk_text_buffer_new(NULL);
	text_view = GTK_TEXT_VIEW(gtk_builder_get_object(xml, "textview"));
	
	gtk_text_view_set_buffer(GTK_TEXT_VIEW(text_view), buffer);
	
	// Get widgets from glade
	window = GTK_WIDGET(gtk_builder_get_object(xml, "window"));
	
	// customize window
	gtk_window_set_title(GTK_WINDOW(window), "Text Converter");
	gtk_window_set_resizable(GTK_WINDOW(window), FALSE);
	gtk_window_set_position(GTK_WINDOW(window), GTK_WIN_POS_CENTER);
	gtk_window_set_icon(GTK_WINDOW(window), logo);
	
	// connect signals
	gtk_builder_connect_signals(xml, NULL);
	g_signal_connect(G_OBJECT(window), "destroy", G_CALLBACK(destroy), NULL);	
	
	// Combo Box
	cbox = GTK_COMBO_BOX(gtk_builder_get_object(xml, "combobox"));
	
	GtkCellRenderer *cell;
	GtkListStore *store;

	store = gtk_list_store_new (1, G_TYPE_STRING);
	gtk_combo_box_set_model(cbox, GTK_TREE_MODEL (store));
	//combo_box = gtk_combo_box_new_with_model (GTK_TREE_MODEL (store));
	g_object_unref (store);

	cell = gtk_cell_renderer_text_new ();
	gtk_cell_layout_pack_start (GTK_CELL_LAYOUT (cbox), cell, TRUE);
	gtk_cell_layout_set_attributes (GTK_CELL_LAYOUT (cbox), cell,
									  "text", 0,
									  NULL);
	
	
	//gtk_combo_box_insert_text(cbox, 0, "sfg");
	gtk_combo_box_set_title(cbox, "Choose encoding mode");
	
	//xml
	xmlParse(cbox, xmlFileName);
	
	// combo box active title
	gtk_combo_box_set_active(cbox, 0);
	
	gtk_widget_show_all (window);
	gtk_main ();

	return 0;
}
